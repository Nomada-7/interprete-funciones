
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests;
import junit.framework.TestCase;
import org.junit.Before;
import principal.Expresion;
/**
 *
 * @author
 */
public class TestExpresionSimple extends TestCase{
    
    @Before
    public void testExprecionSimple(){
        String f = "32 + 3";
        Expresion expre = new Expresion(f);
        assertTrue(expre.calcular()== 35.0);
    }
}
