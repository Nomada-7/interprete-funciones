/*
 * Copyright 2018 nomada-7 contacto: gitnomada@gmail.com.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package interprete.elementos;

public class Operador extends Elemento {

    private static char[] operadores = {'+', '-', '*', '/', '^', '(', ')'};

    private char op;
    private int valor;
    
    public static boolean esOperador(String simbolo){
        int i= 0;
        
        while(i< operadores.length){
            
            if(simbolo.equalsIgnoreCase(""+operadores[i])){
                return true;
            }
            
            i++;
        }
        return false;
    }
    
    public Operador(char op) {
        this.op = op;
        cargarV(op);
    }

    @Override
    public Character getElemento() {
        return this.op;
    }

    public static boolean esParentesis(char a) {
        boolean es = false;
        if (('(' == a) || (')' == a)) {
            es = true;
        }
        return es;
    }

    public boolean esPCerrado() {
        return op == ')';
    }

    public boolean esPAbierto() {
        return op == '(';
    }

    public boolean esParentesis() {
        return esPAbierto() || esPCerrado();
    }

    public boolean esMayor(Operador o) {
        return this.valor > o.valor;
    }

    public double evaluar(Numero a, Numero b) {
        double resultado = 0.0;
        double numA = a.getElemento();
        double numB = b.getElemento();

        switch (op) {
            case '*':
                resultado = numA * numB;

                break;
            case '/':
                resultado = numA / numB;

                break;
            case '+':
                resultado = numA + numB;

                break;
            case '-':
                resultado = numA - numB;

                break;
            case '^':
                resultado = Math.pow(numA, numB);
        }
        return resultado;
    }

    private void cargarV(char o) {
        switch (o) {
            case '^':
                this.valor = 400;

                break;
            case '*':
                this.valor = 300;

                break;
            case '/':
                this.valor = 300;

                break;
            case '+':
                this.valor = 200;

                break;
            case '-':
                this.valor = 200;

                break;
            case '(':
                this.valor = 6;
        }
    }

    @Override
    public boolean esOperador() {
        return true;
    }
}
