/*
 * Copyright 2018 nomada-7 contacto: gitnomada@gmail.com.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package interprete.elementos;

public class Numero extends Elemento {

    public static double p = Math.PI;
    public static double e = Math.E;

    private Double num;
    private String var;

    public static boolean esNumero(String valor) {
        try {

            Double.parseDouble(valor);

            return true;

        } catch (NumberFormatException | NullPointerException ex) {

            return false;
        }

    }

    public static boolean esVariable(String valor) {

        if (valor.length() == 1) {
            char letra = valor.charAt(0);
            
            return Character.isLetter(letra);

        }
        return false;
    }

    public Numero(Double num) {
        this.num = num;
    }

    public Numero(String var) {
        this.num = null;
        this.var = var;

    }

    public void valorParaIndefinido(Double n) {
        this.num = n;
    }

    @Override
    public Double getElemento() {
        return this.num;
    }

    public String getVariable() {
        return this.var;
    }

    public boolean esNumero() {
        return true;
    }

}
