/*
 * Copyright 2018 nomada-7 contacto: gitnomada@gmail.com.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package interprete.elementos;

import expciones.NoExisteFuncion;

public class Funcion extends Elemento {

    private String funcionG;
    private int posicion;
    private static String[] funciones = {"sen", "cos", "tan", "arcsen", "arccos", "arctan", "log", "ln", "raiz"};

    public Funcion(String fun) throws NoExisteFuncion {
        this.funcionG = fun;
        ubicar(fun);
    }

    public String getElemento() {
        return this.funcionG;
    }

    public double evaluar(Double x) {
        double sale;
        switch (posicion) {
            case 0: {
                sale = sen(x);
            }
            break;
            case 1: {
                sale = cos(x);
            }
            break;
            case 2: {
                sale = tan(x);
            }
            break;
            case 3: {
                sale = arcsen(x);
            }
            break;
            case 4: {
                sale = arccos(x);
            }
            break;
            case 5: {
                sale = arctan(x);
            }
            break;
            case 6: {
                sale = log(x);
            }
            break;
            case 7: {
                sale = ln(x);
            }
            break;
            case 8: {
                sale = raiz(x, 0.5);
            }
            break;
            default:
                sale = 0;
        }

        return sale;
    }

    public double sen(Double x) {
        return Math.sin(x);
    }

    public double cos(Double x) {
        return Math.cos(x);
    }

    public double tan(Double x) {
        return Math.tan(x);
    }

    public double ln(Double x) {
        return Math.log(x);
    }

    public double log(Double x) {
        return Math.log10(x);
    }

    private double raiz(Double b, Double e) {
        return Math.pow(b, e);
    }

    public double arccos(Double x) {
        return Math.acos(x);
    }

    public double arcsen(Double x) {
        return Math.asin(x);
    }

    public double arctan(Double x) {
        return Math.atan(x);
    }

    public static boolean esFuncion(String f) {
        
        int i = 0;
        while (i < funciones.length ) {
            if (funciones[i].equalsIgnoreCase(f)) {
                return true;
            }
            i++;
        }
        return false;
    }

    private int ubicar(String f) throws NoExisteFuncion {
        
        int i = 0;
        
        while (i < funciones.length) {
            if (f.equalsIgnoreCase(funciones[i])) {
                return i;
            }
            i++;
        }
        throw new NoExisteFuncion(f);
    }

    public boolean esFuncion() {
        return true;
    }
}
