/*
 * Copyright 2018 nomada-7 contacto: gitnomada@gmail.com.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package auxiliares;

import interprete.elementos.Elemento;

public class NodoPila
{
  private Elemento elem;
  private NodoPila enlace;

  public NodoPila(Elemento elem)
  {
    this.elem = elem;
  }

  public NodoPila(Elemento elem, NodoPila enlace)
  {
    this.elem = elem;
    this.enlace = enlace;
  }

  public Elemento getElemento()
  {
    return this.elem;
  }

  public void setElemento(Elemento elem)
  {
    this.elem = elem;
  }

  public NodoPila getEnlace()
  {
    return this.enlace;
  }

  public void setEnlace(NodoPila enlace)
  {
    this.enlace = enlace;
  }
}
