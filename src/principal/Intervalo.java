/*
 * Copyright 2018 nomada-7 contacto: gitnomada@gmail.com.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package principal;

import auxiliares.ParOrdenado;
import principal.Expresion;
import java.util.ArrayList;


public class Intervalo {



  public static ArrayList intervaloUnaVariable(double inicio,double fin,double incremento,Expresion ex){
   ArrayList lista=new ArrayList();
       double res;
        while(inicio<=fin){
            res=ex.calcular(inicio);
            lista.add(new ParOrdenado(inicio,res));
            inicio+=incremento;
        }
        return lista;
    }
   public static ArrayList intervaloDosVariable(double inicioX,double finX,double inicioY,double finY,double incremX,double incremY,Expresion ex){
   ArrayList lista=new ArrayList();
       double resX,resY,resZ;
        while(inicioX<=finX){
            if(inicioY<=finY){

            }
            resZ=ex.calcular(inicioX,inicioY);
            lista.add(new ParOrdenado(inicioX,inicioY,resZ));
//            inicio += incremento;
        }
        return lista;
    }
}
