/*
 * Copyright 2018 nomada-7 contacto: gitnomada@gmail.com.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package principal;

import interprete.elementos.Funcion;
import interprete.elementos.Elemento;
import interprete.elementos.Numero;
import interprete.elementos.Operador;


import estructura.*;

public class Expresion {

    private ListExprecion formulaInOr;
    private ListExprecion formulaPosOr;
    private String formulaComparar;

    public Expresion(String f) {
        this.formulaInOr = new ListExprecion();
        this.formulaPosOr = new ListExprecion();
        cargarDatos(f);
    }

    public Elemento elemento(int p) {
        return this.formulaInOr.get(p);
    }

    public void setExpresion(String g) {
        this.formulaInOr.clear();
        this.formulaPosOr.clear();
        cargarDatos(g);

    }

    public int posicion(Elemento e) {
        return this.formulaInOr.indexOf(e);
    }

    public int cantidadElementos() {
        return this.formulaInOr.size();
    }

    public String listaInOr() {
        return this.formulaInOr.toString();
    }

    public String listaPosOr() {
        return this.formulaPosOr.toString();
    }

    /**
     * ingresa al for para ir transformando cada elementod del string en un
     * numero, operador, o una funcion tambien tengo q tener encuenta las
     * variables <code>x</code>,<code>y</code>,<code>z</code> y tambien los
     * numero especiales e,p El primer if lo que hace es concatenar cada
     * elemento del string que pueda formar parte de un numero como un . o si es
     * un digito el el char de la posicion i. Tambien verifica si se trata de
     * los numeros especiales e,p En caso de que no sea un digito, un punto o un
     * numero salta al else, donde pregunto si el elemento anterior era un
     * numero o uno de los numeros especiales y asi vaciar o no la variable aux
     * asi se carga el numero. Despues pregunta en que elemento esta parado. Si
     * es un operador pregunto si es un menos. La idea es, cuando el menos esta
     * delante de un numero que lo concatene a ese proximo numero que viene asi
     * genero un numero negativo. El problema esta en que si el menos esta
     * delante de un parentesis o una funcion lo deje como esta. En el caso de
     * no tratarse de un operador, lo que pregunto es por las variables x,y,z.
     * Si no se trata de ninguna de ellas es porque puede estar parador sobre
     * alguna funcion. Al finalizar el bucle, se revisa si queda algun numero en
     * aux, ya que si el ultimo elemento era un numero y cuando incremento i
     * corto el for, el numero que estaba concatenando se perderia
     *
     *
     */
    private void cargar(String f) {
        this.formulaComparar = f;
        String aux = "", auxD = "";
        char recorrer, anterior, posterior;

        /*
         */
        for (int i = 0; i < f.length(); i++) {
            recorrer = f.charAt(i);
            anterior = (i > 0) ? f.charAt(i - 1) : ' ';
            posterior = (i < f.length() - 1) ? f.charAt(i + 1) : ' ';

            if (Character.isDigit(recorrer) || recorrer == '.' || recorrer == 'e' || recorrer == 'p') {

                auxD = "";
                if (recorrer == 'e') {
                    auxD += Numero.e;
                } else if (recorrer == 'p') {
                    auxD += Numero.p;
                }
                aux += (!auxD.equalsIgnoreCase("")) ? auxD : recorrer;

            } else {

                if ((Character.isDigit(anterior)) || anterior == 'e' || anterior == 'p') {
                    this.formulaInOr.add(new Numero(Double.valueOf(Double.parseDouble(aux))), this.formulaInOr.size() + 1);
                    aux = "";
                }

                if (Operador.esOperador(recorrer)) {
                    if ((recorrer == '-') && ((Character.isDigit(posterior) || posterior == 'e' || posterior == 'p'))) {

                        aux += recorrer;
                        if (anterior != '(') {
                            this.formulaInOr.incertar(new Operador('+'), this.formulaInOr.longitud() + 1);
                        }

                    } else {

                        this.formulaInOr.incertar(new Operador(recorrer), this.formulaInOr.longitud() + 1);

                    }

                } else if ((recorrer == 'x') || (recorrer == 'y') || (recorrer == 'z')) {
                    this.formulaInOr.incertar(new Numero("" + recorrer), this.formulaInOr.longitud() + 1);
                } else {
                    i = cargarFuncion(f, i);
                }
            }
        }
        if (!aux.equals("")) {
            this.formulaInOr.incertar(new Numero(Double.valueOf(Double.parseDouble(aux))), this.formulaInOr.longitud() + 1);
        }
    }

    private int cargarFuncion(String h, int i) {
        int a = i;
        String fo = "";
        while ((h.charAt(a) != ' ') && (h.charAt(a) != '(')) {
            fo = fo + h.charAt(a);
            a++;
        }
        if (Funcion.esFuncion(fo)) {
            this.formulaInOr.incertar(new Funcion(fo), this.formulaInOr.longitud() + 1);
            a--;
        }
        return a;
    }

    private void conversorDeExpresion() {
        PilaElemento auxOp = new PilaElemento();

        int part = this.formulaInOr.cantidadParentesis();
        int i = 1;

        while (this.formulaInOr.longitud() != this.formulaPosOr.longitud() + part) {
            if (i <= this.formulaInOr.longitud()) {
                Elemento e = this.formulaInOr.recuperar(i);
                switch (e.tipo().charAt(0)) {
                    case 'O':
                        Operador o = (Operador) e;
                        if (o.esPCerrado()) {
                            desapilarOp(auxOp);
                        } else {
                            compararOp(auxOp, o);
                        }
                        break;
                    case 'N':
                        this.formulaPosOr.incertar(e, this.formulaPosOr.longitud() + 1);

                        break;
                    case 'F':
                        compararOp(auxOp, e);
                }
                i++;
            } else {
                desapilarOp(auxOp);
            }
        }

    }

    private void desapilarOp(PilaElemento op) {
        boolean corte = true;
        while (corte) {
            if (!op.esVacio()) {
                if (!op.tope().getElemento().equals(Character.valueOf('('))) {
                    Elemento elem = op.desapilar();
                    this.formulaPosOr.incertar(elem, this.formulaPosOr.longitud() + 1);
                } else {
                    corte = false;
                }
            } else {
                corte = false;
            }
        }
        op.desapilar();
    }

    private void compararOp(PilaElemento oP, Elemento op) {
        boolean corte = true;
        if (!oP.esVacio()) {
            Elemento e = oP.tope();
            while ((e.prioridad() >= op.prioridad()) && (corte)) {
                if (op.prioridad() != 6) {
                    e = oP.desapilar();
                    this.formulaPosOr.incertar(e, this.formulaPosOr.longitud() + 1);
                    if (!oP.esVacio()) {
                        e = oP.tope();
                    } else {
                        corte = false;
                    }
                } else {
                    corte = false;
                }
            }
        }
        oP.apilar(op);
    }

    public double calcular() {
        PilaElemento num = new PilaElemento();
        double s = 0.0;
        Numero aux, nuU;

        Elemento e = this.formulaPosOr.recuperar(1), numU, numD;
        num.apilar(e);
        int i = 2;
        while (!num.esVacio()) {
            if (i <= this.formulaPosOr.longitud()) {
                e = this.formulaPosOr.recuperar(i);
                switch (e.tipo().charAt(0)) {
                    case 'N':
                        num.apilar(e);

                        break;
                    case 'O':
                        numD = num.desapilar();
                        numU = num.desapilar();

                        if (numU == null) {
                            nuU = new Numero(0.0);
                        } else {
                            nuU = (Numero) numU;
                        }
                        Operador op = (Operador) e;
                        aux = new Numero((op.evaluar(nuU, (Numero) numD)));
                        num.apilar(aux);

                        break;
                    case 'F':
                        Funcion fn = (Funcion) e;
                        numU = num.desapilar();
                        aux = new Numero(Double.valueOf(fn.evaluar((Double) numU.getElemento())));
                        num.apilar(aux);
                }
                i++;
            } else {
                s = ((Double) num.desapilar().getElemento()).doubleValue();
            }
        }
        return s;
    }

    public double calcular(Double x) {
        PilaElemento num = new PilaElemento();
        double s = 0.0;
        Elemento e = this.formulaPosOr.recuperar(1), numU, numD;
        Numero aux, nuU;

        if (e.getElemento() == null) {
            aux = new Numero(x);
        } else {
            aux = new Numero((Double) e.getElemento());
        }
        num.apilar(aux);
        int i = 2;
        while (!num.esVacio()) {
            if (i <= this.formulaPosOr.longitud()) {
                e = this.formulaPosOr.recuperar(i);
                switch (e.tipo().charAt(0)) {
                    case 'N':
                        if (e.getElemento() == null) {
                            aux = new Numero(x);
                        } else {
                            aux = new Numero((Double) e.getElemento());
                        }
                        num.apilar(aux);

                        break;
                    case 'O':
                        numD = num.desapilar();
                        numU = num.desapilar();
                        if (numU == null) {
                            nuU = new Numero(0.0);
                        } else {
                            nuU = (Numero) numU;
                        }
                        Operador op = (Operador) e;
                        aux = new Numero((op.evaluar(nuU, (Numero) numD)));
                        num.apilar(aux);

                        break;
                    case 'F':
                        Funcion fn = (Funcion) e;
                        numU = num.desapilar();
                        aux = new Numero(Double.valueOf(fn.evaluar((Double) numU.getElemento())));
                        num.apilar(aux);
                }
                i++;
            } else {
                s = ((Double) num.desapilar().getElemento()).doubleValue();
            }
        }
        return s;
    }

    public double calcular(Double x, Double y) {
        PilaElemento num = new PilaElemento();
        double s = 0.0D;
        Elemento e = this.formulaPosOr.recuperar(1), numU, numD;
        Numero aux, auxD, nuU;

        if (e.getElemento() == null) {
            auxD = (Numero) e;
            aux = new Numero((auxD.getVariable().equalsIgnoreCase("x")) ? x : y);
        } else {
            aux = new Numero((Double) e.getElemento());
        }
        num.apilar(aux);
        int i = 2;
        while (!num.esVacio()) {
            if (i <= this.formulaPosOr.longitud()) {
                e = this.formulaPosOr.recuperar(i);
                switch (e.tipo().charAt(0)) {
                    case 'N':
                        if (e.getElemento() == null) {
                            auxD = (Numero) e;
                            aux = new Numero((auxD.getVariable().equalsIgnoreCase("x")) ? x : y);
                        } else {
                            aux = new Numero((Double) e.getElemento());
                        }
                        num.apilar(aux);

                        break;
                    case 'O':
                        numD = num.desapilar();
                        numU = num.desapilar();
                        if (numU == null) {
                            nuU = new Numero(0.0);
                        } else {
                            nuU = (Numero) numU;
                        }
                        Operador op = (Operador) e;
                        aux = new Numero((op.evaluar(nuU, (Numero) numD)));
                        num.apilar(aux);
                        break;
                    case 'F':
                        Funcion fn = (Funcion) e;
                        numU = num.desapilar();
                        aux = new Numero(Double.valueOf(fn.evaluar((Double) numU.getElemento())));
                        num.apilar(aux);
                }
                i++;
            } else {
                s = ((Double) num.desapilar().getElemento()).doubleValue();
            }
        }
        return s;
    }

    private void cargarDatos(String g) {
        cargar(g);
        conversorDeExpresion();
    }

    public boolean esIgual(String o) {
        boolean s;
        s = o.equalsIgnoreCase(this.formulaComparar);
        return s;
    }

}
