/*
 * Copyright 2018 nomada-7 contacto: gitnomada@gmail.com.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package estructura;


import interprete.elementos.Elemento;
import interprete.elementos.Operador;
import java.util.ArrayList;

public class ListExprecion extends ArrayList<Elemento> {

    private int cantidadParentesis;

    public ListExprecion() {
        super();
        cantidadParentesis = 0;
    }

    @Override
    public boolean add(Elemento e) {
        boolean salida = super.add(e);
        addParentesis(e);
        return salida;
    }

    @Override
    public boolean remove(Object o) {
        boolean salida = super.remove(o);
        removeParentesis((Elemento) o);
        return salida;
    }

    public int cantidadParentesis() {
        return cantidadParentesis;
    }

    private void addParentesis(Elemento e) {

        if (e.esOperador()) {
            Operador o = (Operador) e;
            if (o.esParentesis()) {
                cantidadParentesis++;
            }
        }
    }

    private void removeParentesis(Elemento e) {

        if (e.esOperador()) {
            Operador o = (Operador) e;
            if (o.esParentesis()) {
                cantidadParentesis--;
            }
        }
    }
}
