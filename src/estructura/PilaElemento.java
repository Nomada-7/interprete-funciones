/*
 * Copyright 2018 nomada-7 contacto: gitnomada@gmail.com.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package estructura;

import auxiliares.NodoPila;
import interprete.elementos.Elemento;

public class PilaElemento {

    private NodoPila tope;
    private int cantidad;

    public PilaElemento() {
        this.tope = null;
        this.cantidad = 0;
    }

    public boolean apilar(Elemento e) {
        this.tope = new NodoPila(e, this.tope);
        this.cantidad += 1;
        return true;
    }

    public Elemento desapilar() {
        Elemento sacar = null;
        if (!esVacio()) {
            sacar = this.tope.getElemento();
            this.tope = this.tope.getEnlace();
            this.cantidad -= 1;
        }
        return sacar;
    }

    public boolean esVacio() {
        return tope == null;
    }

    public int cantidadElem() {
        return cantidad;
    }

    public Elemento tope() {
        return tope.getElemento();
    }

    public String toString() {
        NodoPila aux = this.tope;
        String s = "";
        while (aux != null) {
            s = s + aux.getElemento().getElemento();
            aux = aux.getEnlace();
        }
        return s;
    }
}
