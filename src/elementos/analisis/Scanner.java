/*
 * Copyright 2018 nomada-7 contacto: gitnomada@gmail.com.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package elementos.analisis;

import java.util.ArrayList;
import java.util.List;

public class Scanner {

    private List<String> desframentador(StringBuilder cadenaEntrante) {

        eliminarEspacios(cadenaEntrante);
        agregarEspacios(cadenaEntrante);

        return obtenerPreTokens(cadenaEntrante);

    }

    private ArrayList<String> obtenerPreTokens(StringBuilder cadena) {

        StringBuilder aux = new StringBuilder();
        ArrayList<String> listaTokens = new ArrayList<>();

        for (int i = 0; i < cadena.length(); i++) {
            char simbolo = cadena.charAt(i);

            if (simbolo != ' ') {

                aux.append(simbolo);

            } else {
                if (aux.length() != 0) {
                    listaTokens.add(aux.toString());
                    aux.delete(0, aux.length());

                }
            }
        }

        return listaTokens;
    }

    private void agregarEspacios(StringBuilder cadena) {

        for (int i = 0; i < cadena.length(); i++) {
            char simbolo = cadena.charAt(i);
            if (simbolo != '.' && !Character.isLetterOrDigit(simbolo) && simbolo != ' ') {

                cadena.insert(i, ' ');
                i++;
                cadena.insert(i + 1, ' ');
            }
        }
        cadena.append(' ');
    }

    private void eliminarEspacios(StringBuilder cadena) {

        for (int i = 0; i < cadena.length(); i++) {
            char simbolo = cadena.charAt(i);

            if (simbolo == ' ') {
                cadena.deleteCharAt(i);
            }
        }
    }
    
    
    public List<Token> getTokens(String cadenaEntrante){
        StringBuilder cadenaBuffer = new StringBuilder(cadenaEntrante);
        
        List<String> lista = desframentador(cadenaBuffer);
        
        return null;
    }
    
    

}
