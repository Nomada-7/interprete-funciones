/*
 * Copyright 2018 nomada-7 contacto: gitnomada@gmail.com.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package elementos.analisis;

import expciones.ValorIncorrecto;
import interprete.elementos.Funcion;
import interprete.elementos.Numero;
import interprete.elementos.Operador;

public class Token {

    public final static int FUNCION = 3, OPERADOR = 2, NUMERO = 1;

    int tipo;
    String valor;

    public Token(String valor) throws ValorIncorrecto {

        this.valor = valor;
        this.tipo = obtenerTipo();
    }

    private int obtenerTipo() throws ValorIncorrecto {

        if (Operador.esOperador(valor)) {
            return OPERADOR;

        } else if (Funcion.esFuncion(valor)) {
            return FUNCION;

        } else if (Numero.esNumero(valor) || Numero.esVariable(valor)) {
            return NUMERO;

        } else {
            throw new ValorIncorrecto(this.valor);
        }
    }

    public int getTipo() {
        return tipo;
    }

    public String getValor() {
        return valor;
    }

    public boolean esFuncion() {
        return FUNCION == tipo;
    }

    public boolean esOperador() {
        return OPERADOR == tipo;
    }

    public boolean esNumero() {
        return NUMERO == tipo;
    }
}
