/*
 * Copyright 2018 nomada-7 contacto: gitnomada@gmail.com.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package elementos;

import interprete.elementos.Elemento;
import interprete.elementos.Operador;
import estructura.ListExprecion;


public class Regla {

    private void revicion(ListExprecion lista) throws Exception {
        int cont = 1;
        char a;
        boolean corte = true;
        Elemento e, eA, eP;
        Operador oA, oP;
        if (0 == lista.cantidadParentesis() % 2) {
            while (cont < lista.size() && corte) {
                e = lista.get(cont);
                eA = (cont > 1) ? lista.get(cont - 1) : null;
                eP = (cont < lista.size()) ? lista.get(cont + 1) : null;;
                a = e.tipo().charAt(0);

                switch (a) {
                    case 'O': {
//
                        if (eA.tipo().equalsIgnoreCase("o") && eP.tipo().equalsIgnoreCase("o")) {
                            oP = (Operador) eP;
                            oA = (Operador) eA;

                            if (!(oA.getElemento() == ')' && oP.getElemento() == '(') || !(oA.getElemento() == ')' && oP.getElemento() == ')') || !(oA.getElemento() == '(' && oP.getElemento() == '(')) {

                            }
                        } else if (2 == 0) {

                        }
                    }
                    break;
                    case 'N': {

                    }
                    break;
                    case 'F': {

                        if (eP.tipo().equalsIgnoreCase("o")) {
                            oP = (Operador) eP;
                        }
                    }
                    break;
                }
                cont++;
            }

        } else {
            throw new Exception("parentesis mal");
        }
    }
}
